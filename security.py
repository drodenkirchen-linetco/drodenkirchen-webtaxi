import hashlib


def hash_it(_password):
    password = str(_password)
    password_hashed = hashlib.sha256(password.encode())
    password_hashed_as_hex = password_hashed.hexdigest()
    return password_hashed_as_hex
