# Author: David Rodenkirchen

from flask import Flask, jsonify, request, send_from_directory, render_template, session
import schichtplaner
import security
import json
import sys

app = Flask(__name__, static_folder='static')
app.secret_key = "CHANGETHISINPRODUCTION"


def current_schichtplan():
    try:
        with open('schichtplan.json') as json_file:
            return json.load(json_file)
    except FileNotFoundError:
        print("Schichtplan Datenbank nicht gefunden")
        sys.exit(1)


def get_logged_in_status():
    if "username" in session:
        return {"logged_in": True, "username": session["username"]}
    else:
        return {"logged_in": False, "username": None}


@app.route('/')
def landing():
    login_status = get_logged_in_status()
    return render_template('index.html', logged_in=login_status["logged_in"], username=login_status["username"])


@app.route('/schichtplan', methods=["POST", "GET"])
def schichtplan_site():
    login_status = get_logged_in_status()
    if request.method == "GET":
        daynight = str(request.args.get('daynight'))
        s_date = str(request.args.get('day')) + str(request.args.get('month')) + str(request.args.get('year'))
        s_data = schichtplaner.get_schichtplan(current_schichtplan(), date=s_date)
        return render_template('schichtplan.html', schichtplan_data=s_data, logged_in=login_status["logged_in"],
                               username=login_status["username"], daynight=daynight)


@app.route('/login', methods=["POST", "GET"])
def login():
    login_status = get_logged_in_status()
    if request.method == "GET":
        if "username" in session:
            session.pop("username", None)
            return render_template('index.html', logged_in=False)
        return render_template('login.html', logged_in=login_status["logged_in"], username=login_status["username"])
    else:
        username = request.form["username"]
        password = security.hash_it(request.form["password"])
        try:
            with open('users.json') as json_file:
                userdata = json.load(json_file)[username]
                if userdata["Passwort"] == password:
                    session["username"] = username
                    session["berechtigung"] = userdata["Berechtigung"]
                    session["kurzzeichen"] = userdata["Kurzzeichen"]
                    err_flag = False
                else:
                    err_flag = True
        except KeyError:
            err_flag = True
        login_status = get_logged_in_status()
        return render_template('login.html', err_flag=err_flag, logged_in=login_status["logged_in"],
                               username=login_status["username"])


@app.route('/project_links')
def project_links():
    login_status = get_logged_in_status()
    return render_template('project_links.html', logged_in=login_status["logged_in"], username=login_status["username"])


@app.route('/change_shift')
def change_shift():
    login_status = get_logged_in_status()
    if not login_status["logged_in"]:
        return ""
    return render_template('change_shift.html', logged_in=login_status["logged_in"], username=login_status["username"])


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
