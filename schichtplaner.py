import json
import datetime

ANZAHL_TAXEN = 21


# Nimmt ein Schichtplan Dict und ein Datum und gibt den Schichtplan für den ausgewählten Tag zurück
# Wenn noch kein Schichtplan existiert, wird er erstellt

def get_schichtplan(schichtplan, date=None):  # Format (D)D(M)MYYYY
    if date is None or _check_date_valid(date) is False:
        date = str(datetime.datetime.now().strftime("%d%m%Y"))
    if date not in schichtplan:
        new_schichtplan = {date: {}}
        for taxi in range(ANZAHL_TAXEN):
            new_schichtplan[date]["Taxi {}".format(taxi + 1)] = {"timeslots": {}}
            time = "0000"
            for _ in range(96):
                if str(time)[-2:] == "45":
                    time = int(time) + 55
                else:
                    time = int(time) + 15
                if len(str(time)) == 2:
                    new_schichtplan[date]["Taxi {}".format(taxi + 1)]["timeslots"][
                        "00{}".format(str(time))] = "frei"
                elif len(str(time)) == 3:
                    new_schichtplan[date]["Taxi {}".format(taxi + 1)]["timeslots"][
                        "0{}".format(str(time))] = "frei"
                else:
                    new_schichtplan[date]["Taxi {}".format(taxi + 1)]["timeslots"][str(time)] = "frei"
        schichtplan.update(new_schichtplan)
        with open('schichtplan.json', 'w') as json_file:
            json.dump(schichtplan, json_file, indent=4)
    return schichtplan[date]


def _check_date_valid(date):
    try:
        _ = datetime.datetime.strptime(date, '%d%m%Y')
    except ValueError:
        return False
    return True
